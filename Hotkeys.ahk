﻿/**
 *  @author 李洪超
 *  所有自定义的快捷键
 *  2014-12-05
 */

;win=#,Ctrl=^,Alt=!,Shift=+

PROGRAMDIR:= "D:\Program Files (x86)\AutoHotkey\myHotkeys_LHC\ShortCuts"


^!#R::Reload
^#R::Reload
;^#S::Run, C:\windows\system32\SnippingTool.exe  ;截图工具
;^#J::Run, calc  ;计算器
;AlwaysOnTop win+A***************************************************
#A::		;窗口置顶，toggle即切换状态
WinGet, active_id, ID, A
WinSet, AlwaysOnTop, Toggle, ahk_id %active_id%
return
;********************************************************************
^!P::Run, %PROGRAMDIR%\PicPick


^#V::Run, %PROGRAMDIR%\viatc,,Min	;vim方式的tc插件

^#Y::FileRecycleEmpty 		;Run, nircmd emptybin
^#T::Run, taskmgr
^#G::Run, gpedit.msc
^#M::Run, compmgmt.msc
;************************************************************
/*
!#R::				;获取文件属性								;这一条及以下一条尚不能实现打开多个文件
send, ^c
clipwait
filename = %clipboard%
subpart = /
StringGetPos, index, filename, %subpart%, R
if index >= 0
{
  filename := SubStr(filename, 1,index+1)
  clipboard = %filename%
}
Run, properties %clipboard%
return
*/
;用notepad++编辑选定文件,***************************************
/*
^#O::
send, ^c
clipwait
filename = %clipboard%
subpart = /
StringGetPos, index, filename, %subpart%, R
if index >= 0
{
  filename := SubStr(filename, 1,index+1)
  clipboard = %filename%
}
Run, D:\Program Files (x86)\Notepad++\notepad++.exe %clipboard%
return
*/
;***************************************************************
;#K::

;WinGet, active_id, ID, A			;只能关闭非管理员权限的
;WinClose, ahk_id %active_id%		;正常关闭窗口，winkill为强制关闭

;Send !{F4}
;return									;不要用了，很容易在锁屏的时候误操作关闭窗口
;***************************************************************
^#I::Run, inetcpl.cpl       ;internet属性
^#U::Run, ncpa.cpl          ;网络和共享中心
^#A::Run, appwiz.cpl        ;uninstall programs




;*****************************************************************
^#J:: ;快速进入注册表指定位置
send ^c
        ; 复制选中的文字
clipwait
        ; 等待复制动作的完成
StringReplace, clipboard, clipboard, ＼,　\, All
        ; 网络一些文章很不严谨，“＼”“\”不分。替换掉剪贴板中所有的“＼”，并且再把替换后的文字发送到剪贴板。
RegWrite, REG_SZ, HKEY_CURRENT_USER, Software\Microsoft\Windows\CurrentVersion\Applets\Regedit, LastKey, Computer\%clipboard%
        ; 把负责注册表最近打开的键值修改为剪贴板中的路径。参数 REG_SZ 就是右上角图片中 LastKey 的类型。HKEY_CURRENT_USER 就是目标注册表分支，接着的参数是目标路径，然后是要修改的键，最后是要修改的值。
run regedit
        ; 运行注册表
return
;关闭显示器************************************************************
^#L::
Sleep 1000 ; Give user a chance to release keys (in case their release would wake up the monitor again).
; Turn Monitor Off:
SendMessage, 0x112, 0xF170, 2,, Program Manager  ; 0x112 is WM_SYSCOMMAND, 0xF170 is SC_MONITORPOWER.
; Note for the above: Use -1 in place of 2 to turn the monitor on.
; Use 1 in place of 2 to activate the monitor's low-power mode.
return

;倒计时休眠************************************************************
^#!H::
InputBox, time,Timing Hibernate,计时休眠，请输入时间，（单位为分钟）\n Please enter a number (mi
MsgBox, 4,休眠,将在%time%分钟后休眠，是否继续？
IfMsgBox, No
{
	MsgBox, 0, Hibernate canceled, 休眠已取消, 3
	return
}
else
{
	MsgBox, 0,休眠,将在%time%分钟后休眠，可随时从系统托盘退出脚本以取消休眠计时, 3
	time := time*60000

	Sleep,%time%
	DllCall("PowrProf\SetSuspendState", "int", 1, "int", 0, "int", 0)
	return
}


;电源操作：**************************************************************
^#!+L::Shutdown, 0 		;注销	Logoff

^#!+S::					;睡眠（不是混合睡眠）
; Call the Windows API function "SetSuspendState" to have the system suspend or hibernate.
; Parameter #1: Pass 1 instead of 0 to hibernate rather than suspend.
; Parameter #2: Pass 1 instead of 0 to suspend immediately rather than asking each application for permission.
; Parameter #3: Pass 1 instead of 0 to disable all wake events.
DllCall("PowrProf\SetSuspendState", "int", 0, "int", 0, "int", 0)
return

^#!+H::					;休眠，立即休眠
DllCall("PowrProf\SetSuspendState", "int", 1, "int", 0, "int", 0)
return

^#!+R::Shutdown, 2		;重启
/********************************************************************

窗口操作
*/
^#Left::
WinGetActiveTitle, wintitle
;MsgBox, the active window's id is "%wintitle%".
WinGetPos, Xpos, Ypos, Width, Height, %wintitle% 		; "A" to get the active window's pos.
;MsgBox, the postion of the window is %Xpos%`,%Ypos%.
;Xpos:=Xpos-10
;MsgBox, now xpos is %Xpos%
WinMove, %wintitle%, , Xpos-20, Ypos, %Width%, %Height%,
return

^#Right::
WinGetActiveTitle, wintitle
WinGetPos, Xpos, Ypos, Width, Height, %wintitle%
WinMove, %wintitle%, , Xpos+20, Ypos, %Width%, %Height%,
return

^#Up::
WinGetActiveTitle, wintitle
WinGetPos, Xpos, Ypos, Width, Height, %wintitle%
WinMove, %wintitle%, , Xpos, Ypos-20, %Width%, %Height%,
return

^#Down::
WinGetActiveTitle, wintitle
WinGetPos, Xpos, Ypos, Width, Height, %wintitle%
WinMove, %wintitle%, , Xpos, Ypos+20, %Width%, %Height%,
return

/*常用程序************************************************************
*/
!#F::Run, %PROGRAMDIR%\Foxmail

!#N::Run, %PROGRAMDIR%\notepad++
!#S::Run, %PROGRAMDIR%\VSCode

!#C::Run, %PROGRAMDIR%\Cmder

;Vim
::/vim::
Run, %PROGRAMDIR%\gVim
return

;MathType
::/mt::
Run, %PROGRAMDIR%\MathType
return

;Pint.NET
::/pn::
Run, %PROGRAMDIR%\PaintNET
return

;飞信
::/fx::
DetectHiddenWindows, On
ifWinExist, 飞信
{
	WinShow, 飞信
	WinActivate	;, ahk_exe Fetion.exe
}
else
{
	Run, %PROGRAMDIR%\fx
}
;Run, %PROGRAMDIR%\fx
DetectHiddenWindows, off
return

;QQ
::/qq::
DetectHiddenWindows, On
IfWinExist ahk_exe QQ.exe
{
	
	WinActivate ;, ahk_exe QQ.exe
}
else
{
Run, %PROGRAMDIR%\QQ
}
DetectHiddenWindows, off
return

;WinEdt
::/winedt::
Run, %PROGRAMDIR%\WinEdt 9
return

;utorrent
::/ut::
Run, %PROGRAMDIR%\utorrent
return

;Gimp
::/gimp::
Run, %PROGRAMDIR%\GIMP
return

;foobar2000
::/fb::
Run, %PROGRAMDIR%\foobar2000
return

;ditto
::/ditto::
run, %PROGRAMDIR%\ditto
return

::/everything::
Run,%PROGRAMDIR%\Everything
return

::/listary::
Run, %PROGRAMDIR%\Listary
return

::/s4a::
Run, %PROGRAMDIR%\SciTE4AutoHotkey
return


::/gridmove::
Run, %PROGRAMDIR%\GridMove
return


::/cygwin::
Run, %PROGRAMDIR%\Cygwin64
return


::/emacs::
Run, %PROGRAMDIR%\Emacs
return


::/cb::
Run, %PROGRAMDIR%\CodeBlocks
return


::/chrome::
Run, %PROGRAMDIR%\Google Chrome
return


::/ie::
Run, %PROGRAMDIR%\Internet Explorer
return


::/ps::
Run, %PROGRAMDIR%\Photoshop
return


::/picasa::
Run, %PROGRAMDIR%\Picase3
return


::/matlab::
Run, %PROGRAMDIR%\MATLAB
return


::/wiz::
Run, %PROGRAMDIR%\WizNote
return


::/tc::
Run, %PROGRAMDIR%\Total Commander 64 Bit
return


::/foxmail::
Run, %PROGRAMDIR%\foxmail
return


::/lispbox::
Run, %PROGRAMDIR%\lispbox
return


::/eclipse::
Run, %PROGRAMDIR%\Eclipse
return


::/wingide::
Run, %PROGRAMDIR%\Wing IDE 5
return


::/procexp::
Run, %PROGRAMDIR%\ProcExp
return


::/rstudio::
Run, %PROGRAMDIR%\RStudio
return


::/vs::
Run, %PROGRAMDIR%\Visual Studio
return

;Lingoes
::/lingoes::
Run, %PROGRAMDIR%\Lingoes
return

;Xilinx ISE
::/ise::
Run, %PROGRAMDIR%\ISE Design Suite
return

;sublime
::/sublime::
Run, %PROGRAMDIR%\sublime
return